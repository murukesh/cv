CHROME := google-chrome-stable
UNAME_S := $(shell uname -s)

ifeq ($(UNAME_S),Darwin)
	CHROME = /Applications/Google Chrome.app/Contents/MacOS/Google Chrome
endif

all: cv-two-page-x.pdf

%-x.pdf: %.html
	"${CHROME}" --headless --disable-gpu --run-all-compositor-stages-before-draw --print-to-pdf-no-header --print-to-pdf=$*.tmp.pdf file:///$$PWD/$<
	gs -q -dNOPAUSE -dBATCH -dCompatibilityLevel=1.5 -dPDFSettings=/prepress -dEmbedAllFonts=true -dCompressFonts=true -dSubsetFonts=true -sPAPERSIZE=a4 -sDEVICE=pdfwrite -r600 -g4960x7015 -dPDFFitPage -sOutputFile=$@ -f $*.tmp.pdf cv.pdfmarks
	rm $< $*.tmp.pdf

%.html: %.md cv.css cv.html
	pandoc -s -f markdown $< -t html --template=cv.html --css cv.css --strip-comments --self-contained -M pdf=true -o $@

