---
title: CV (Murukesh Mohanan)
lang: en-GB
name:
  common: Murukesh Mohanan (ムル)
  姓: Mullasseril
  名: Murukesh
  セイ: ムッラセレル
  メイ: ムルケシュ
phone: +81 80 7746 8828
email: murukesh.mohanan@gmail.com
github:
  personal: muru
  work: murukesh-mohanan-paidy
url: muru.dev

---

## [Paidy, Inc.]{.org} [Tokyo]{.location} [Platform Engineer]{.role} [May 2021 -- present]{.time}
### Production Engineering
#### Change Management and Infrastructure Control
- Rotation on production change management process and on-call duty
- Manage and upgrade IaC for AWS (Terraform deployed on CircleCI)
- Maintain internal tooling in Python using CircleCI, Jira, PagerDuty APIs
- Evaluate new services (Terraform Cloud, JupiterOne) to improve process
- International team with members currently in Japan, India, and EMEA

## Community
### Stack Overflow
#### Extensive contributions to Stack Overflow and other Stack Exchange sites
- 190,000+ reputation in Ask Ubuntu, 70,000+ in Unix & Linux
- Over 4000 answers posted in the past 9 years
- <https://stackexchange.com/users/1042873/muru?tab=top>
<!--
- Moderator *pro tempore* of Vi & Vim, April 2015 – April 2017
-->

## [NABLAS, Inc.]{.org} [Tokyo]{.location} [Software Engineer]{.role} [July 2019 -- April 2021]{.time}

### iLect v2 and PyGrade
#### Development of e-learning and training platforms

- Part of backend team using Python (Flask) and Kubernetes API
- Developed a scaling system in Go complementing GKE Autoscaler
- Codified cloud infra for both GCP and AWS in Terraform
- Deployed on EKS, GKE, and Azure using GitHub Actions and Cloud Build

### ML Research Platform
#### Set up and maintain computing infrastructure for R&D team

- JupyterHub on a cluster of on-premise GPU boxes using Docker Swarm
- Configuration using Ansible, monitoring using Prometheus and Grafana
- Install and manage VPN (OpenVPN), intranet DNS (BIND)

## [Yahoo! JAPAN, Inc.]{.org} [Tokyo]{.location} [DevOps Engineer]{.role} [October 2016 -- June 2019]{.time}

### NoSQL Team
#### Setup and administration of Cassandra database clusters on CentOS

- Develop distributed monitoring system (Go daemon, Python client)
- Automation of cluster management (Chef, Fabric, Screwdriver on Jenkins)
- Test deployment on self-managed Kubernetes on OpenStack VMs

### Open Source
#### Apache Cassandra

- Minor contributions to Cassandra-related projects (CCM and drivers)
- Mentored Yahoo! JAPAN interns in contributing to OSS in 2017 and 2018

### Talks given
#### Cassandra and Scylla at Yahoo! JAPAN

- Presented at Distributed Data Summit 2018 and Scylla Summit 2018
- Overview of Cassandra usage at Y!J and results of testing ScyllaDB

## [Dept. of Computer Science, IIT Bombay]{.org} [Mumbai]{.location} [July 2013 -- July 2016]{.time}
### System Administrator
#### Part-time, 20 hrs/week

- Enforced management of configuration via git (etckeeper) and Puppet
- Managed dozens of on-premise servers, both physical and virtual (KVM)
- Set up an instance of GitLab for department usage

<!--
- Created a Debian repository for distributing in-house tools and config
- Split up monolithic web server for reliability, security and performance
- Migrated team from to LDAP group-based access control
-->

## Volunteer projects

### <https://mlsearch.ai> (Machine Learning Tokyo)
#### Website for searching and sharing ML resources [July 2020 -- December 2022]{.time}
- Part of an international group of volunteers based in Europe and Asia
- To be launched and open-sourced by late 2022, currently in public beta
- Backend development in Python (Flask), frontend in React
- Deployed using AWS Lambda over S3, API Gateway, and DynamoDB
- Set up IaC using Terraform and CD using GitHub Actions

## Education

|                     |                                              |             |              |
|:--------------------|:---------------------------------------------|:------------|-------------:|
| IIT Bombay, India   | M. Tech. in Computer Science and Engineering | (GPA: 8.37) | 2013 -- 2016 |
| IIT Guwahati, India | B. Tech. in Mechanical Engineering           | (GPA: 6.69) | 2008 -- 2012 |

## Other professional experience
### Application Developer {.role}
#### iNautix Technologies India Pvt. Ltd., Chennai. [June 2012 -- June 2013]{.time}
- Maintenance of legacy C back-end and migration to C++ and Java

## Skills

### Natural languages
- English
- Japanese (N2)

### Programming languages
- Fairly well: Python, Bash, Go, Awk
- Somewhat: JavaScript, C++, Java

### Technologies
- Recent: Terraform, Docker, Kubernetes, Ansible
- Past: OpenStack, Puppet, Chef, Debian/RPM packaging

## Academic projects
### RSA: Side Channel Attacks [Master's project]{.note}
#### Prof. Bernard Menezes, IIT Bombay [Autumn 2015 -- Spring 2016]{.time} {.prof}
Implemented the Flush+Reload side-channel attack against the RSA public-key cryptographic algorithm, to extract the
private key by monitoring the time taken to perform various operations involved in RSA, augmented using lattice algorithms

<!--
### True Modal Control [Bachelor's project]{.note}
#### Dr. K. Kalita, IIT Guwahati [Autumn 2011 -- Spring 2012]{.time} {.prof}
Development of a control theory for direct control of the modes of vibration of
a system, which can have arbitrary damping, (even gyroscopic).

- Used Structure Preserving Equivalences to decouple the quadratic pencil representing the system
- Extend the method to handle singular system matrices
- Implemented in MATLAB
-->

<!--

### RAFT protocol
#### Dr. S. Sriram, IIT Bombay [Spring 2016]{.time} {.prof}
Distributed computing course project: implement the RAFT consensus protocol.

- Wrote a toy distributed filesystem in Go, with a TDD approach

### Enhancement of the Bodhitree e-learning platform
#### Bhaskar Raman, IIT Bombay [Spring 2014]{.time} {.prof}
Usability and functionality enhancements to a Django/React project:

- Question highlighting based on attempt status
- Links to load videos at an arbitrary point

### Internship
#### Indian Oil Corporation Ltd. Refinery, Guwahati [Summer 2011]{.time}
Vocational Training: Observation and study of various sections of the refinery:
- Crude Distillation Unit,
- Thermal Power Station, etc.

### Emotion detection from live chat
#### Pushpak Bhattacharya, IIT Bombay [Autumn 2014]{.time} {.prof}
Developed an application in Python to detect emotions from live chat
- Processed and cleaned raw dataset to obtain relevant data points
- Analyzed and compared performance of various classifiers like  \hspace*{0.7cm}Naive Bayes, Linear SVC, Vector Space Model

### Study of OSSEC HIDS
#### Bernard Menezes, IIT Bombay [Autumn 2014]{.time} {.prof}
We configured the department servers and lab systems for usage of
OSSEC, a host-based intrusion detection system.
- Configured rules to protect and alert against potential malicious activity
- Performed a comparative study of OSSEC features

## Personal Website
### <https://muru.dev>
#### Developed using Jekyll and GitLab ([`murukesh/murukesh.gitlab.io`](https://gitlab.com/murukesh/murukesh.gitlab.io))

-->
